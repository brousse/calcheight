#!/data/data/com.termux/files/usr/bin/python
# coding: utf8

#author brouss@tutanota.com
#web	https://framagit.org/brousse/calcheight/tree/master
#ver	0.0
#start	21 dec 2018
#end	???
#licens GPL V3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

from os import system
from subprocess import getoutput
#from time import time,strptime,mktime,strftime,gmtime,sleep
#from sys import argv

#TODOs:


#system and globals. Dont fuck lightly

#for "pretty" GPS coordinates display
def decTo60(val,axis):
    if val<0:
        val=-val
        ax=axis[1]
    else:
        ax=axis[0]
    va,vb=divmod(val,1)
    vb,vc=divmod(vb*60,1)
    vc*=60
    return("%d°%d'%s\"%s"%(int(va),int(vb),str(vc)[:7],ax))

def smscompliant(msg):
    if "\\" in msg:
        msg="\\\\\\\\".join(msg.split("\\"))
    if "(" in msg:
        msg="\\(".join(msg.split("("))
    if ")" in msg:
        msg="\\)".join(msg.split(")"))
    if "\n" in msg:
        msg="\\\\n".join(msg.split("\n"))
    if "'" in msg:
        msg="\\'".join(msg.split("'"))
    if "\"" in msg:
        msg="\\\"".join(msg.split("\""))
    if "&" in msg:
        msg="\\&".join(msg.split("&"))
    return(msg)

def readResult(res,fields,multi=False):
    res=res.split("\n")
    ret={}
    array=None
    realfil=""
    for line in res:
        line=line.strip()
        if line[-1]==",":
            line=line[:-1]
            line=line.strip()
        if ":" in line:
            fil,val=line.split(":",1)
            fil=fil.strip()
            if fil in fields:
                val=val.strip()
                if val=="{":
                    array=[]
                    realfil=fil
                if val[0]=="\""==val[-1]:
                    val=val[1:-1]
                elif "." in val:
                    val=float(val)
                else:
                    val=int(val)
        elif array!=None:
            if 
            

        
###Funcs related to termux functionnality (sms, contacts)

#send an sms
def send(msg,nb):
    try:
        cmd="termux-sms-send -n %s %s"%(nb,smscompliant(msg))
        print("\n"+cmd+"\n")
        system(cmd)
    except:
        print("Error while writing to %s"%nb)
        return(False)
    return(True)

#read gps position via network (instead of gps supposedly more precise 
# but could make it work so far)
def readNetworkPos():
    try:
        lines=getoutput("termux-location -p network -r once").split("\n")
    except:
        print("Crashed while determining location."+ \
            "Is location allowed generally? specifically to termux-api?")
        return(unknown_pos)
    lat=lon=0
    for line in lines:
        if ":" in line:
            fil,val=line.split(":",1)
            fil=fil.strip()
            if "latitude" in fil:
                lat=val[:-1].strip()
            if "longitude" in fil:
                lon=val[:-1].strip()
    if lat and lon:
        return(lat+" "+lon)
    print("Could not determine location. Could be an accident. ")
    return(unknown_pos)

def getNewSms():
    global dones,launch_time,pos_max_age,pos,pos_time
    global err_count,err_count0
    name=ok=whok=nb=0
    try:
        lines=getoutput("termux-sms-list -l %d"%nb_sms_read).split("\n")
        err_count=err_count0
    except:
        lines=getoutput("termux-sms-list -l %d"%nb_sms_read).split("\n")
        err_count-=1
        if err_count<=0:
            exit("Cant access SMS. I need SMS and contact list rights.")
        lines=[]
    for line in lines:
        if ":" in line:
            fil,val=line.split(":",1)
            fil=fil.strip()
            if "sender" in fil:      #info comes from contact list
                name=val[2:-2]
            if "body" in fil:      #SMS content. Seek keyword.  note "-1" below
                ok=(keyword==val[2:-1]) # -1 bc no ",": this is the last field
            if "received" in fil:   #Received_time
                try:
                    whok=mktime(strptime(val[2:-2],fmt))
                except:
                    print("invalid received time: "+val[2:-2])
                    whok=0
            if "number" in fil:     #calling number
                nb=val[2:-2]
        elif "}" in line:           #denotes end of an SMS
            print(name,ok,whok,nb,keyword==val[2:-1].strip())   #debug
            if isOkay(nb,name,whok,ok):
                delta = time()-pos_time
                if delta > pos_max_age/(2.0 if pos==unknown_pos else 1) :
                    print("refresh pos")
                    pos=readNetworkPos()
                    pos_time=time()
                    msgPos=buildMsgPos(pos,pos_time,12)
                print(msgPos) 
                msg=buildMsgHead(name)+msgPos+buildMsgTail()
                if send(msg,nb):
                    print("msg sent to "+name)
                else:
                    print("Failed to msg "+name)
                dones[nb]=whok
            name=ok=whok=nb=0
